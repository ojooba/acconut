# Book Chapter Companion Repository
This repository contains the accompanying Jupyter notebooks for the book chapter titled *A Tutorial on Machine Learning and Data Science Tools with Python*, pp. 435–480, Machine Learning for Health Informatics, Volume 9605, Lecture Notes in Computer Science, Springer, 2016. ISBN: 978-3-319-50477-3.

See <http://link.springer.com/chapter/10.1007/978-3-319-50478-0_22>. 

## Notebooks

**Notebooks are currently being updated and will be uploaded in the coming days**

The following table indicates which notebook accompanies which section of the book chapter:

| Section | Pages   | Section Title                              | Notebook                                         |
|---------|---------|--------------------------------------------|--------------------------------------------------|
| 7.2     | 448–450 | NumPy                                      | [NumPy.ipynb](notebooks/NumPy.ipynb)                       |
| 7.3     | 450–456 | Pandas                                     | [Pandas.ipynb](notebooks/Pandas.ipynb)                     |
| 8       | 456–457 | Data Visualisation and Plotting            | [Plotting.ipynb](notebooks/Plotting.ipynb)                 |
| 9.2     | 458–462 | Linear Regression                          | [LinearRegression.ipynb](notebooks/LinearRegression.ipynb) |
| 9.3     | 462–467 | Non-Linear Regression and Model Complexity | Not yet live.                                    |
| 9.4     | 467–468 | Clustering                                 | Not yet live.                                    |
| 9.5     | 468–469 | Classification                             | Not yet live.                                    |
| 9.6     | 470–472 | Dimensionality Reduction                   | Not yet live.                                    |
| 10      | 472–476 | Neural Networks and Deep Learning          | Not yet live.                                    |

